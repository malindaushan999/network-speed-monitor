﻿using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Text;

namespace NetSpeedMon.Common
{
    public static class SharedData
    {
        public static double DownloadSpeed;
        public static double UploadSpeed;
        public static bool BackgroundTransparency = true;
        public static double BackgroundOpacity = 0.7;
        public static string BackgroundColor = "#000";
        public static List<NetworkInterface> NetworkAdapterList = new();
        public static NetworkInterface SelectedNetworkAdapter;
    }
}
