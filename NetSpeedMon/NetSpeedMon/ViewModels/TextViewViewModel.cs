﻿using NetSpeedMon.Common;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.NetworkInformation;
using System.Threading;

namespace NetSpeedMon.ViewModels
{
    public class TextViewViewModel : BindableBase, INavigationAware
    {
        private string _downSpeed;
        private string _upSpeed;

        public string DownSpeed
        {
            get { return _downSpeed; }
            set { SetProperty(ref _downSpeed, value); }
        }

        public string UpSpeed
        {
            get { return _upSpeed; }
            set { SetProperty(ref _upSpeed, value); }
        }

        public TextViewViewModel()
        {

        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
            //throw new NotImplementedException();
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
         //   throw new NotImplementedException();
        }

        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            DownSpeed = SharedData.DownloadSpeed.ToString("F");
            UpSpeed = SharedData.UploadSpeed.ToString("F");
        }
    }
}
