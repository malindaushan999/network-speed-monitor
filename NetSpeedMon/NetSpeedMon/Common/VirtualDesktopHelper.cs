﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace NetSpeedMon.Common
{
    [ComImport]
    [Guid("a5cd92ff-29be-454c-8d04-d82879fb3f1b")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IVirtualDesktopManager
    {
        bool IsWindowOnCurrentVirtualDesktop(IntPtr topLevelWindow);

        Guid GetWindowDesktopId(IntPtr topLevelWindow);

        void MoveWindowToDesktop(IntPtr topLevelWindow, ref Guid desktopId);
    }

    public static class CLSID
    {
        public static Guid VirtualDesktopManager { get; } = new Guid("aa509086-5ca9-4c25-8f95-589d3c07b48a");
    }

    internal class ComObjects
    {
        public IVirtualDesktopManager VirtualDesktopManager { get; private set; }

        public ComObjects()
        {
            this.VirtualDesktopManager = (IVirtualDesktopManager)Activator.CreateInstance(Type.GetTypeFromCLSID(CLSID.VirtualDesktopManager));
        }
    }

    internal static class ComInterface
    {
        public static IVirtualDesktopManager VirtualDesktopManager
            => new ComObjects().VirtualDesktopManager;
    }

    public static class VirtualDesktopHelper
    {
        /// <summary>
        /// Determines whether this window is on the current virtual desktop.
        /// </summary>
        /// <param name="hWnd">The handle of the window.</param>
        public static bool IsCurrentVirtualDesktop(IntPtr hWnd)
        {
            return ComInterface.VirtualDesktopManager.IsWindowOnCurrentVirtualDesktop(hWnd);
        }

        /// <summary>
		/// Moves a window to the specified virtual desktop.
		/// </summary>
		/// <param name="hWnd">The handle of the window to be moved.</param>
		/// <param name="virtualDesktop">The virtual desktop to move the window to.</param>
		public static void MoveToDesktop(IntPtr hWnd, IntPtr hWnd2)
        {
            var guid = ComInterface.VirtualDesktopManager.GetWindowDesktopId(hWnd2);

            ComInterface.VirtualDesktopManager.MoveWindowToDesktop(hWnd, ref guid);
        }
    }
}
