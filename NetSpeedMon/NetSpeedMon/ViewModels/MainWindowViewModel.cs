﻿using NetSpeedMon.Common;
using NetSpeedMon.Views;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Regions;
using Prism.Services.Dialogs;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net.NetworkInformation;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;

namespace NetSpeedMon.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        private readonly IRegionManager _regionManager;
        private readonly IDialogService _dialogService;
        private BackgroundWorker _networkSpeedCheckWorker;

        #region Bindings

        // Properties
        private NetworkInterface _selectedNetworkAdapter;
        private ObservableCollection<MenuItem> _networkAdapterList = new();
        private double _backgroundOpacity = SharedData.BackgroundOpacity;
        private bool _backgroundTransparency = SharedData.BackgroundTransparency;
        private string _backgroundColor = SharedData.BackgroundColor;

        public NetworkInterface SelectedNetworkAdapter
        {
            get { return _selectedNetworkAdapter; }
            set
            {
                SetProperty(ref _selectedNetworkAdapter, value);
            }
        }

        public ObservableCollection<MenuItem> NetworkAdapterList
        {
            get { return _networkAdapterList; }
            set
            {
                SetProperty(ref _networkAdapterList, value);
            }
        }

        public double BackgroundOpacity
        {
            get { return _backgroundOpacity; }
            set
            {
                SetProperty(ref _backgroundOpacity, value);
            }
        }

        public bool BackgroundTransparency
        {
            get { return _backgroundTransparency; }
            set
            {
                SetProperty(ref _backgroundTransparency, value);
            }
        }

        public string BackgroundColor
        {
            get { return _backgroundColor; }
            set
            {
                SetProperty(ref _backgroundColor, value);
            }
        }

        // Commands
        public DelegateCommand LoadedCommand { get; set; }
        public DelegateCommand SettingsCommand { get; set; }
        public DelegateCommand ExitCommand { get; set; }
        public DelegateCommand<object> SelectNICommand { get; set; }
        public DelegateCommand RefreshNIListCommand { get; set; }

        #endregion Bindings

        public MainWindowViewModel(IRegionManager regionManager, IDialogService dialogService)
        {
            // Initialize injected services
            _regionManager = regionManager;
            _dialogService = dialogService;

            // Initialize commands
            LoadedCommand = new DelegateCommand(LoadedExecute);
            SettingsCommand = new DelegateCommand(SettingsExecute);
            ExitCommand = new DelegateCommand(ExitExecute);
            SelectNICommand = new DelegateCommand<object>(SelectNIExecute);
            RefreshNIListCommand = new DelegateCommand(RefreshNIListExecute);

            NetworkChange.NetworkAddressChanged += NetworkChange_NetworkAddressChanged;

            SelectedNetworkAdapter = NetworkInterface.GetAllNetworkInterfaces()
                .Where(x => x.NetworkInterfaceType == NetworkInterfaceType.Wireless80211)
                .Where(x => x.OperationalStatus == OperationalStatus.Up)
                .FirstOrDefault();

            InitNetworkAdapterList();

            _networkSpeedCheckWorker = new BackgroundWorker();
            _networkSpeedCheckWorker.DoWork += NetSpeedCheckWorker_DoWork;
            _networkSpeedCheckWorker.RunWorkerAsync();
        }

        private void RefreshNIListExecute()
        {
            InitNetworkAdapterList();
        }

        private void SettingsExecute()
        {
            _dialogService.ShowDialog(nameof(SettingsView));
        }

        private void NetworkChange_NetworkAddressChanged(object sender, EventArgs e)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                InitNetworkAdapterList();
            });
        }

        private void SelectNIExecute(object obj)
        {
            var selectedNI = NetworkInterface.GetAllNetworkInterfaces().Where(x => x.Id == obj.ToString()).FirstOrDefault();
            if (selectedNI != null)
            {
                SharedData.SelectedNetworkAdapter = selectedNI;
                SelectedNetworkAdapter = selectedNI;
                NetworkAdapterList.ToList().ForEach(x => x.IsChecked = false);
                NetworkAdapterList.FirstOrDefault(x => x.CommandParameter == obj).IsChecked = true;
            }
        }

        private void InitNetworkAdapterList()
        {
            NetworkAdapterList.Clear();
            SharedData.NetworkAdapterList.Clear();
            SharedData.NetworkAdapterList.AddRange(NetworkInterface.GetAllNetworkInterfaces());

            foreach (var item in NetworkInterface.GetAllNetworkInterfaces())
            {
                MenuItem menuItem = new()
                {
                    Header = item.Name,
                    IsCheckable = true,
                    Command = SelectNICommand,
                    CommandParameter = item.Id
                };
                if (item.Id == SelectedNetworkAdapter?.Id)
                {
                    menuItem.IsChecked = true;
                    SharedData.SelectedNetworkAdapter = item;
                }

                NetworkAdapterList.Add(menuItem);
            }

            MenuItem mnuItem = NetworkAdapterList.FirstOrDefault(x => x.CommandParameter.ToString() == SelectedNetworkAdapter?.Id);
            if (mnuItem == null)
            {
                SelectedNetworkAdapter = null;
                SharedData.SelectedNetworkAdapter = null;
            }
        }

        private void ExitExecute()
        {
            Application.Current.Shutdown();
        }

        private void LoadedExecute()
        {
            _regionManager.RequestNavigate("ContentRegion", "TextView");
        }

        private void NetSpeedCheckWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            while (Application.Current != null)
            {
                if (SelectedNetworkAdapter != null && SelectedNetworkAdapter.OperationalStatus == OperationalStatus.Up)
                {
                    try
                    {
                        var reads_Received = Enumerable.Empty<double>();
                        var reads_Sent = Enumerable.Empty<double>();
                        var sw = new Stopwatch();
                        var lastBytesReceived = SelectedNetworkAdapter.GetIPv4Statistics().BytesReceived;
                        var lastBytesSent = SelectedNetworkAdapter.GetIPv4Statistics().BytesSent;

                        for (var i = 0; i < 10; i++)
                        {
                            sw.Restart();
                            Thread.Sleep(100);
                            var elapsed = sw.Elapsed.TotalSeconds;
                            var bytesReceived = SelectedNetworkAdapter.GetIPv4Statistics().BytesReceived;
                            var bytesSent = SelectedNetworkAdapter.GetIPv4Statistics().BytesSent;

                            var local_Recieved = (bytesReceived - lastBytesReceived) / elapsed;
                            lastBytesReceived = bytesReceived;
                            var local_Sent = (bytesSent - lastBytesSent) / elapsed;
                            lastBytesSent = bytesSent;

                            // Keep last 20, ~2 seconds
                            reads_Received = new[] { local_Recieved }.Concat(reads_Received).Take(20);
                            reads_Sent = new[] { local_Sent }.Concat(reads_Sent).Take(20);

                            if (i % 2 == 0)
                            { // ~1 second
                                var bSec_Recieved = reads_Received.Sum() / reads_Received.Count();
                                var kbs_Received = bSec_Recieved * 8 / 1024;
                                var bSec_Sent = reads_Sent.Sum() / reads_Sent.Count();
                                var kbs_Sent = bSec_Sent * 8 / 1024;
                                SharedData.DownloadSpeed = kbs_Received;
                                SharedData.UploadSpeed = kbs_Sent;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        if (ex is NetworkInformationException || ex is NullReferenceException)
                        {
                            SharedData.DownloadSpeed = 0;
                            SharedData.UploadSpeed = 0;
                        }
                        Console.WriteLine(ex.Message);
                    }
                }
                else
                {
                    SharedData.DownloadSpeed = 0;
                    SharedData.UploadSpeed = 0;
                }

                if (Application.Current == null) break;

                Application.Current?.Dispatcher.Invoke(() =>
                {
                    BackgroundOpacity = SharedData.BackgroundOpacity;
                    //BackgroundTransparency = SharedData.BackgroundTransparency;
                    BackgroundColor = SharedData.BackgroundColor;
                    SelectedNetworkAdapter = SharedData.SelectedNetworkAdapter;
                    _regionManager.RequestNavigate("ContentRegion", "TextView");
                    if (ScreenHelper.IsForegroundFullScreen()) Application.Current.MainWindow?.Hide();
                    else Application.Current.MainWindow?.Show();
                });
            }
        }

    }
}
