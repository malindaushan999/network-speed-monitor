﻿using NetSpeedMon.ViewModels;
using NetSpeedMon.Views;
using Prism.Ioc;
using System.Windows;

namespace NetSpeedMon
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        protected override Window CreateShell()
        {
            return Container.Resolve<MainWindow>();
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<TextView, TextViewViewModel>();
            containerRegistry.RegisterForNavigation<GraphView>();
            containerRegistry.RegisterDialog<SettingsView, SettingsViewViewModel>();
        }
    }
}
