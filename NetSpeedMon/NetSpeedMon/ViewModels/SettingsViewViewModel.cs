﻿using NetSpeedMon.Common;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.NetworkInformation;

namespace NetSpeedMon.ViewModels
{
    public class SettingsViewViewModel : BindableBase, IDialogAware
    {
        public string Title => "Settings - NSM";

        #region Bindings

        // Properties
        private int _backgroundOpacity;
        private bool _backgroundTransparency;
        private string _backgroundColor;
        private ObservableCollection<NetworkInterface> _networkAdapterList = new();
        private string _selectedNetworkAdapterId;

        public int BackgroundOpacity
        {
            get { return _backgroundOpacity; }
            set
            {
                SetProperty(ref _backgroundOpacity, value);
                SharedData.BackgroundOpacity = value / 100.00;
            }
        }

        public bool BackgroundTransparency
        {
            get { return _backgroundTransparency; }
            set
            {
                SetProperty(ref _backgroundTransparency, value);
                //SharedData.BackgroundTransparency = value;
            }
        }

        public string BackgroundColor
        {
            get { return _backgroundColor; }
            set
            {
                SetProperty(ref _backgroundColor, value);
                SharedData.BackgroundColor = BackgroundColor;
            }
        }

        public ObservableCollection<NetworkInterface> NetworkAdapterList
        {
            get { return _networkAdapterList; }
            set
            {
                SetProperty(ref _networkAdapterList, value);
            }
        }

        public string SelectedNetworkAdapterId
        {
            get { return _selectedNetworkAdapterId; }
            set
            {
                SetProperty(ref _selectedNetworkAdapterId, value);
                SharedData.SelectedNetworkAdapter = NetworkInterface.GetAllNetworkInterfaces().FirstOrDefault(x => x.Id == value);
            }
        }

        // Commands
        public DelegateCommand CancelCommand { get; set; }

        #endregion Bindings

        #region IDialogAware

        public event Action<IDialogResult> RequestClose;

        public bool CanCloseDialog()
        {
            return true;
        }

        public void OnDialogClosed()
        {
            //throw new NotImplementedException();
        }

        public void OnDialogOpened(IDialogParameters parameters)
        {
            NetworkAdapterList.Clear();
            NetworkAdapterList.AddRange(SharedData.NetworkAdapterList);

            SelectedNetworkAdapterId = SharedData.SelectedNetworkAdapter?.Id;

            BackgroundOpacity = (int)(SharedData.BackgroundOpacity * 100);
            BackgroundTransparency = SharedData.BackgroundTransparency;
            BackgroundColor = SharedData.BackgroundColor;
        }

        #endregion IDialogAware

        public SettingsViewViewModel()
        {
            CancelCommand = new DelegateCommand(CancelExecute);
        }

        private void CancelExecute()
        {
            SharedData.BackgroundOpacity = BackgroundOpacity / 100.00;
            SharedData.BackgroundTransparency = BackgroundTransparency;
            SharedData.BackgroundColor = BackgroundColor;

            DialogResult dialogResult = new();
            RequestClose.Invoke(dialogResult);
        }
    }
}
